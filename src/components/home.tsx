import React from 'react';
import Styled from 'styled-components';
import styled from 'styled-components';
import 'font-awesome/css/font-awesome.min.css';
import Detail from './detail';
import Axios from 'axios';




const NavBar = Styled.div`
margin-top: 30px;
font-size: 1.5em;
text-align: left;
border: 1px solid black;
width: 880px;
`;
export interface AppState {
    show: boolean;
    faverit: boolean;
    isFaverit: boolean;
    data: Array<any>;
    forDetails: any;
    styles: Array<any>
    styleFilteration: Array<any>
    country: Array<any>
    status: Boolean
    counter: any
}
class HomePage extends React.Component<{}, AppState>{
    state = {
        show: false,
        faverit: false,
        isFaverit: false,
        data:[],
        forDetails: [],
        styles: [] as any,
        styleFilteration: [],
        country: [],
        status: false,
        counter: 0
    }
    changePage = (status:boolean)=> {
        this.setState({show: status})
    }
    getData = async ()=>{
        let result = await Axios({
            url: 'http://localhost:3001/graphql',
            method: 'post',
            data: {
              query: `
              {
                beers{
                    id
                    title
                    style_id
                    country_id
                    styles{
                      id
                      name
                    }
                    countries{
                      id
                      name
                    }
                    description
                    favorite_beers{
                        id
                        beer_id
                    }
                  }
                  
                }
                `
            }
          });

          this.setState({data: result.data.data.beers,styleFilteration:result.data.data.beers},()=>{ 
          let counter:any =  this.state.data.filter((data:any)=>{
                return typeof(data.favorite_beers[0]) == 'undefined' ? '' : data.favorite_beers[0].beer_id
           })
            // let counter:any = typeof(img.favorite_beers[0]) == 'undefined' ? '' : img.favorite_beers[0].beer_id
            console.log(counter.length)
            this.setState({counter: counter.length})
        
        })
       
    }
    getStyle = async ()=>{
        let result = await Axios({
            url: 'http://localhost:3001/graphql',
            method: 'post',
            data: {
              query: `
              {
                styles {
                    id
                    name
                  }
                }
                `
            }
          });
        //   let add : any = {name:'Style Filter',id:'0'} ;
          let rt : string[] = [];
        //   rt.push(add)
          this.setState({styles: result.data.data.styles})  
    }
    getCountry = async ()=>{
        let result = await Axios({
            url: 'http://localhost:3001/graphql',
            method: 'post',
            data: {
              query: `
              {
                countries {
                    id
                    name
                  }
                }
                `
            }
          });
        
        //   let rt : string[] = [];
        //   console.log(result.data.data.countries)
          this.setState({country: result.data.data.countries})  
    }
   
    componentDidMount(){
        this.getData();
        this.getStyle();
        this.getCountry();
    }
    bd = styled.div`
    dir: center;
    margin-left:250px;  
`;

    StyleFilter = styled.select`
        margin-top: 10px;
        margin-left: 20px;
        font-size: 1.5em;
        text-align: left;
        border-style: solid;
    `;
    Options = styled.option`
        font-size: 1em;
    `;

    Head = styled.div`
        display: block;
        margin-bottom: 20px;
    `;

    Image = styled.div`
    border: 1px dashed black;
    border-radius: 10px;
    text-align: right;
    height: 200px;
    width: 200px;
    display:inline-block;
    margin: 0px 10px;
    background-image:url( https://via.placeholder.com/300);
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
  `;
  Faverit = styled.div`
    right: 10px;
    top: 0px;
  margin:20px;
  position: absolute;
  color: black,
  cursor: pointer;
  `;
  unFaverit = styled.i`
    right: 10px;
    top: 0px;
  margin:20px;
  position: absolute;
  color: white
  `;

  ImageName = styled.p`
    font-size: 1.5em;
    display: block;
    padding: 0px;
    margin: 5px 10px 30px 10px;
  `;

    styleFilter = (e: any)=>{
        if(e == 0){
            this.setState({styleFilteration: this.state.data})

        }else if(e == 1){
           let oneData = this.state.data.filter((data:any)=>{
                if(data.style_id == 1){
                    return data;
                }
            })
            this.setState({styleFilteration: oneData}) 

        }else if (e == 2){
           let towData = this.state.data.filter((data:any)=>{
                if(data.style_id == 2){
                    return data;
                }
            })
            this.setState({styleFilteration: towData})

        }else if(e == 3){
           let threeData = this.state.data.filter((data:any)=>{
                if(data.style_id == 3){
                    return data;
                }
            })
            this.setState({styleFilteration: threeData})
        }  
    }
    countryFilter = (e: any)=>{
        if(e == 0){
            this.setState({styleFilteration: this.state.data})
      
        }else if(e == 1){
           let oneData = this.state.data.filter((data:any)=>{
                if(data.country_id == 1){
                    return data;
                }
            })
            this.setState({styleFilteration: oneData})
      
        }else if (e == 2){
           let towData = this.state.data.filter((data:any)=>{
                if(data.country_id == 2){
                    return data;
                }
            })
            this.setState({styleFilteration: towData})
      
        }else if(e == 3){
               
           let threeData = this.state.data.filter((data:any)=>{
                if(data.country_id == 3){
                    return data;
                }
            })
            this.setState({styleFilteration: threeData})
         
        }
    
        
    }
  
    Text = styled.div`
        display: inline-block;
    `;

    Count = styled.p`
    text-align: left;
    margin-left: 300px;
    display: inline;
    `;
    fav = async (id:any) => {
        console.log('fav',id)
        let result = await Axios({
            url: 'http://localhost:3001/graphql',
            method: 'post',
            data: {
            query: `
                
            mutation{
                createFavorite_beers(id:${id},beer_id:"${id}"){
                  id
                  beer_id
                }
              }
                
                `
            }
            
          });
        //   console.log(result.data)
          this.getData();
    }

    unFav = async (id:any) => {
       
        let result = await Axios({
            url: 'http://localhost:3001/graphql',
            method: 'post',
            data: {
            query: `
                
            mutation{
                createUnFavorite_beers(id:${id},beer_id:"${id}"){
                  id
                  beer_id
                }
              }
                
                `
            }
            
          });
        //   console.log(result.data)
          this.getData();
    }

    HomePage = () => {
        
        return (
          <this.bd>
            <React.Fragment>
                <NavBar>Beerfiy</NavBar>
                <this.Head>
                    <this.StyleFilter onChange={(e)=> this.styleFilter(e.target.value)}>
                        {this.state.styles.map((data:any,i: any)=>{
                          return  <this.Options key={i} value={data.id}>{data.name}</this.Options>

                        })}
                    </this.StyleFilter>
                    <this.StyleFilter onChange={(e)=> this.countryFilter(e.target.value)}>
                        {this.state.country.map((data:any,i:any)=>{
                            return <this.Options key={i} value={data.id}>{data.name}</this.Options>

                        })}
                    </this.StyleFilter>
                    <this.Count>You have {this.state.counter} Faverit Beers</this.Count>
                </this.Head>
                {this.state.styleFilteration.map((img: any,i:any)=>{
               
                    return (
                        <React.Fragment key={i}>
                            <this.Text>   
                                <this.Image  onClick={()=> this.setState({show: true,isFaverit: true,forDetails: img})} >
                                  { typeof(img.favorite_beers[0]) == 'undefined' ? 
                                  <this.unFaverit  onClick={(e)=>  this.fav(img.id)}><i className="fa fa-heart"></i></this.unFaverit> 
                                  :<this.Faverit  onClick={(e)=> this.unFav(img.id)} ><i className="fa fa-heart"></i></this.Faverit>}  
                                </this.Image>
                                <this.ImageName >{img.styles[0].name}</this.ImageName>
                            </this.Text>
                        </React.Fragment>
                    )
                })}
                
               
                
            </React.Fragment>
            </this.bd>
        )
    }
    Detail = ()=>{
        return  <Detail img={this.state.forDetails} changePage={this.changePage} show={this.state.show}></Detail>
    };
   
    render(){
        return this.state.show ? this.Detail() : this.HomePage();
    }
}

export default HomePage;
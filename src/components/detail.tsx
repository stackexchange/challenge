import React from 'react';
import Styled from 'styled-components';


const NavBar = Styled.div`
    font-size: 1.5em;
    text-align: left;
    border-style: solid;

`;
interface HomePageState {
    show: boolean;
    changePage: CallableFunction;
    [img: string]: any;
    // country: String;
    // style: String;
}

class Detail extends React.Component<HomePageState> {
    state = {
        img: [],
        country:'',
        style:'',
    }
    
    getCountryAndStyle = ()=>{
        let img:any = this.props.img;
        // console.log(Object.values(this.props.img))
        let style: any = Object.values(img)[4]
        let okStyle : any = Object.values(style).filter((data: any,i:any)=> {
            return data
        })[0]
        let styelName: any = Object.values(okStyle)[1]
        console.log(Object.values(okStyle)[1])

        let country: any = Object.values(img)[5]
        let okCountry : any = Object.values(country).filter((data: any,i:any)=> {
            return data
        })[0]
       
        let countryName: any = Object.values(okCountry)[1]
        this.setState({country:countryName,style: styelName })
    }
    componentDidMount(){
        console.log(Object.keys(this.props.img)[0],Object.values(this.props.img)[0])
        this.getCountryAndStyle()
    }
    body = ()=>{
        let keys = (Object.values(this.props.img))
        let values1 = (Object.values(this.props.img))
        
            // console.log(Object.keys(this.props.img)[2],Object.values(this.props.img)[2])
            return(
            <this.Container>
                        <this.List>
                            <this.Item>Name: {Object.values(this.props.img)[1]}</this.Item>
                            <this.Item>Style: {this.state.style}</this.Item>
                            <this.Item>Country: {this.state.country} </this.Item>
                        </this.List>
                        <this.Info>{Object.values(this.props.img)[6]}.</this.Info>
                        {/* <this.List>
                            <this.Item>Name: {Object.values(this.props.img)[1]}</this.Item>
                            <this.Item>Style: {Object.values(this.props.img)[3]}</this.Item>
                            <this.Item>Country: {Object.values(this.props.img)[4]}</this.Item>
                        </this.List>
                        <this.Info>{Object.values(this.props.img)[2]}.</this.Info> */}
            </this.Container>
            )
      
    }
    Head = Styled.div`
        display: block;
        margin: 25px;
        display: block;
        &:hover{
            cursor: pointer;
        } 
    `;

    Image = Styled.div`
        height: 420px;
        width: 400px;
        float: left;
        display:inline-block;
        margin-left: 25px;
        background-image: url(https://via.placeholder.com/300);
        background-repeat: no-repeat;
        background-size: cover;
    `;

    Container = Styled.div`
       
        display: inline;
        float: left; 
    `;
    Clear = Styled.div`
        clear: both;
    `;
    List = Styled.ul`
        
        styel: none;
        display: block;
    `;

    Item = Styled.li`
        display: block;
        font-size: 20px;
        margin: 10px;
    `;

    Info = Styled.p`   
       
        font-size: 20px;
        margin: 50px;
        width: 600px;
        display: block;
    `;
    Parent = Styled.div`
        
    `;
    i = 0
    render(){
       let i = 0;
        return (
            <React.Fragment>
                <NavBar>Beerfiy</NavBar>
                <this.Head onClick={()=>this.props.changePage(false)}>Beerfiy / {this.state.style} /  {this.state.country}</this.Head>
                <this.Parent>
                    <this.Image></this.Image>
                   {this.body()}
                    
                    <this.Clear></this.Clear>
                    </this.Parent>
            </React.Fragment>
            
        )
    }
}

export default Detail;
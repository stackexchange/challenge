var lorem =
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ornare sapien a nisi egestas lacinia. Donec nibh felis, luctus eget dolor ac, aliquet vulputate tellus. Donec nec pretium tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'

module.exports = {
	beers: [
		{
			id: 1,
			title: 'Singha',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 2,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 3,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 4,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 5,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 6,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 7,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 8,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 9,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 10,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 11,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 12,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 13,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 14,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 15,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 16,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 17,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 18,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 19,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 20,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 21,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 22,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 23,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 24,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 25,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 26,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://via.placeholder.com/300'
		},
		{
			id: 27,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://via.placeholder.com/300'
		}
	],
	favorite_beers: [
		{ id: 1, beer_id: 1 },
		{ id: 2, beer_id: 2 },
		{ id: 2, beer_id: 5 },
		{ id: 2, beer_id: 10 },
		{ id: 2, beer_id: 13 },
		{ id: 2, beer_id: 4 },
	],
	styles: [
		{ id: 0, name: 'Style Filter' },
		{ id: 1, name: 'Pale Lager' },
		{ id: 2, name: 'Wheat beer' },
		{ id: 3, name: 'Imperial Stout' }
	],
	countries: [
		{ id: 0, name: 'Country Filter' },
		{ id: 1, name: 'Thailand' },
		{ id: 2, name: 'United States' },
		{ id: 3, name: 'Germany' }
	]
}

const db = require('./db')
const beers = require('./data/db')
const { DataStore } = require('notarealdb');
const fs = require('fs');
const store = new DataStore('./data');
let data = store.collection(beers.beers);
const Query = {
    test: () => 'Test Success, GraphQL server is up & running !!',
    beers: ()=> beers.beers,
    styles: ()=> beers.styles,
    countries: () => beers.countries,
    favorite_beers: ()=> beers.favorite_beers,
    beerById: async (root,args,context,info) => {
       let beerid = args.id;
       return beers.beers.filter((data)=> data.id == beerid)[0]
    },
    students:()=> db.students.list(),
    studentById:(root,args,context,info) => {
        //args will contain parameter passed in query
        return db.students.get(args.id);
     }
}
const Beers = {
    fullTitle: (root,args,context,info)=>{
        return root.title+":"+root.id
    },
    styles: (root)=>{
        return beers.styles.filter((data)=> {
            if(root.style_id == data.id){
                return root
            }
        })
    },
    countries: (root)=>{
        return beers.countries.filter((data)=>{
            if(root.country_id == data.id){
                return root
            }
        })
    },
    favorite_beers: (root)=>{
        return beers.favorite_beers.filter((data)=>{
            if(root.id == data.beer_id){
                return root
            }
        })
    },
    

}

const Mutation = {
    createFavorite_beers:(root,args,context,info) => {
        // console.log('ok')
        let add = {id: args.id,beer_id: args.beer_id}
        // return args;
        beers.favorite_beers.push(add)
        // fs.writeFileSync('./data',add);
        // let json = JSON.parse(add);
        console.log(beers.favorite_beers.push(add))
        // fs.appendFile('data.json', JSON.stringify(json), function (err) {
        //     if (err) throw err;
            // console.log(add);
        //   });
        return args;
    },
    
    createUnFavorite_beers:(root,args,context,info) => {
        console.log('ok')
        let add = {id: args.id,beer_id: args.beer_id}
        // return args;
        beers.favorite_beers = beers.favorite_beers.filter((data)=> data.beer_id != args.beer_id)
        // fs.writeFileSync('./data',add);
        // let json = JSON.parse(add);
        // console.log(beers.favorite_beers.push(add))
        // fs.appendFile('data.json', JSON.stringify(json), function (err) {
        //     if (err) throw err;
            // console.log(add);
        //   });
        return args;
    },
    
}

 module.exports = {Query,Beers,Mutation}